# HTTP Request Logger

See https://www.drupal.org/project/request_logger

Extends and overrides the core DB Logger service to add HTTP request data to the logs.

Especially useful for decoupled sites, where you need to see what POST requests caused PHP exceptions or other 5xx
errors.

## Usage

- Install and enable the module.
- Configuration:
  After successful installation, browse to the HTTP Request Logger settings page either by using the "Configure" link
  (next to module's name on the modules listing page), or page's menu link under:
  Home » Administration » Configuration » Development » HTTP Request Logger Path:
  `/admin/config/development/request_logger`
- By default, all log entries with a severity of ERROR or higher (i.e. RFC 5424 numerical code lower than or equal to 3)
  will include extra information about the HTTP request in the DB logs at `/admin/reports/dblog`. You may configure this
  threshold on the aforementioned page.
