<?php

namespace Drupal\request_logger\Logger;

use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\dblog\Logger\DbLog;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Override the code DbLog class to add custom logging functionality.
 *
 * @package Drupal\request_logger\Logger
 */
class RequestLogger extends DbLog {

  use RfcLoggerTrait;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new DefaultSubscriber object.
   */
  public function __construct(
    Connection $connection,
    LogMessageParserInterface $parser,
    RequestStack $request_stack
  ) {
    $this->requestStack = $request_stack;
    parent::__construct($connection, $parser);
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    $config = \Drupal::config('request_logger.settings');

    if ($level > $config->get('threshold')) {
      // Just use the default logger. Don't log anything extra.
      parent::log($level, $message, $context);
      return;
    }

    $request = $this->requestStack->getCurrentRequest();

    // Add info about the HTTP request not normally included in the DB logs.
    $context['@request_headers'] = json_encode($request->headers->all(), JSON_PRETTY_PRINT);
    $context['@request_query'] = $request->getQueryString();
    $context['@request_content'] = $request->getContent();
    $message .= <<<REQUEST
<dl>
  <dt>Headers</dt>
  <dd><pre>@request_headers</pre></dd>
  <dt>Query</dt>
  <dd><pre>@request_query</pre></dd>
  <dt>Content</dt>
  <dd><pre>@request_content</pre></dd>
</dl>
REQUEST;

    parent::log($level, $message, $context);
  }

}
