<?php

namespace Drupal\request_logger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Implements a settings form for Reroute Email configuration.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'request_logger_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['request_logger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('request_logger.settings');

    $form['threshold'] = [
      '#type' => 'select',
      '#title' => $this->t('Logging level'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('threshold'),
      '#description' => $this->t('The minimum severity (maximum numerical code) for which to log requests.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('request_logger.settings')
      ->set('threshold', $form_state->getValue('threshold'))
      ->save();
  }

}
